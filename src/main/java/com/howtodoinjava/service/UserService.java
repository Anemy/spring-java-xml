package com.howtodoinjava.service;

import com.howtodoinjava.model.User;

public interface UserService {
    public User getUser(int id);
}
