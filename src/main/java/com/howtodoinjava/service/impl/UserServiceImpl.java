package com.howtodoinjava.service.impl;

import com.howtodoinjava.dao.UserDao;
import com.howtodoinjava.model.User;
import com.howtodoinjava.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public User getUser(int id) {
        return userDao.getUser(1);
    }
}
