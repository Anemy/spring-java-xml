package com.howtodoinjava.dao.impl;

import com.howtodoinjava.dao.UserDao;
import com.howtodoinjava.model.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {
    @Override
    public User getUser(int id) {
        return new User("userName" + id, "lastName");
    }
}
