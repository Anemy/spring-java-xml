package com.howtodoinjava.dao;

import com.howtodoinjava.model.User;

public interface UserDao {
    public User getUser(int id);
}
