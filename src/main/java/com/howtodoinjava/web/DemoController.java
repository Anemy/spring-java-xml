package com.howtodoinjava.web;

import java.util.ArrayList;

import com.howtodoinjava.model.User;
import com.howtodoinjava.model.Users;
import com.howtodoinjava.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DemoController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/{value}", method = RequestMethod.GET, produces = {"application/json"})
    @ResponseBody
    public User returnValue(@PathVariable int value) {
        System.out.println(userService.getUser(value));
        return userService.getUser(value);
    }
}
